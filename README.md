# practice_latex

LaTeXでレポート等を自由自在に書けるようになるまで練習する。  
毎日の進捗をREADMEに記録して、頑張る。

## 進捗

- 2021-04-14
	- うーん、眠い！笑
	- まあ、多分、結構、おそらく、色々やりました！
- 2021-04-13
	- ま、色々やりました。ウェブサイトの整備などなど。
	- 思ったけど、これ最早日記と化してるな。いいけど。
	- コンビーフ美味しい
		- 今日はそのまま食べたけど、今度は料理のお兄さんのコンビーフボムでも作るか
- 2021-04-12
	- 今日は数学の勉強しています。えらい！
	- 青チャート、素晴らしい！
- 2021-04-11
	- `GitHub Actions` について勉強していたので許して。。。
- 2021-04-10
	- LaTeXは使ってないけど線形代数の勉強は今からするから許して。。。
- 2021-04-09
	- 何もしてねえええええええええええ！
- 2021-04-08
	- 今日は情報数学Aの課題の回答をLaTeXで書いて提出した
	- 楽！
- 2021-04-07
	- 七分くらいまで進めました。まあ、気が向いたときにやる感じで。
	- 昨日はともかく今日は遊びすぎたので反省
	- 明日はがっつり勉強する
- 2021-04-06
	- 04-07の自分より：仕方なかったんです。疲れてたんです。許して。。。
- 2021-04-05
	- まあ入学式の日なので進捗は少ないと思ったり
	- `\usepackage` を `\documentclass` より前に書いたらダメらしいです！
		- `LaTeX Error: \usepackage before \documentclass.`
		- 新たな知見を得られるの楽しいんだが？
	- `\chapter` は `jsbook` などでは使えるが `jsarticle` では使えないらしい
	- 文中に数式的なアレ（矢印など）を入れるなら `$` で囲む必要があるらしい
	- [こんな感じのPDFが出力できた](https://twitter.com/mopisec/status/1378739870349189120)
- 2021-04-04
	- このリポジトリを作成した
	- [TeXインストーラ3](http://www.ms.u-tokyo.ac.jp/~abenori/soft/abtexinst.html)を使ってTeXの環境構築をしようとしたが、WORDはTeX Live使ってるらしいのでやめた
	- TeX Liveのisoをここから落としてインストールした
		- ファイルサイズ大きくない？？？ :thinking_face:
	- WORDの `article-template` 触ってるけど、普通にWindows対応してないな...?
		- `make` は死ぬし、専用のエディタからPDF作ろうとしても死ぬし、`だめだこりゃ(笑)、`
		- せっかく `docker-compose.yml` 用意してくれてるけど、VMware関係のためにHyper-V周りの設定を弄ってるから使えない :sob: 
	- 仕方がないので（トラブルシューティングの時間がもったいないので）UbuntuマシンにTeX Live入れるか
		- WinのTeX Liveは消した
		- あ＾～、楽。`sudo apt install texlive-full`
		- `nvim` で編集作業しようかな～....最近すっかり `Notepad++` みたいなGUIエディタにはまってしまったのだ
		- というか、Ubuntuマシンへのインストールの方が5000兆倍速い気がするのだが......SUGEEEEEE
		- あーあ。悲しいね。[cs20210404_1.txt](cs20210404_1.txt)
	- もう面倒なのでDocker使います...... (on Ubuntuマシン)
		- まずさっき入れたTeX Liveさんを消します....... Done
		- `sudo docker-compose up`します ....... Done
			- `article_build_1  | Latexmk: All targets (main.pdf) are up-to-date`
		- `scp [REDACTED]:~/article-template/main.pdf main.pdf`
	- `main.pdf` ... いや、WORDだよ。どうみてもWORDですよ。最高。